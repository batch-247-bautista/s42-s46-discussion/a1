const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const userController = require('../controllers/userController');

router.post('/', auth.verify, (req, res) => {
  const data = {
    userId: req.body.userId,
    productId: req.body.productId,
  };
  const userData = auth.decode(req.headers.authorization);
  userController.order(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
