const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// Route for create a product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	try {
		productController.getAllProduct(data).then(resultFromController => res.send(resultFromController));
	} catch (err) {
		res.status(401).send(err.message);
	}
});

// Route for retrieving all active products
router.get("/active", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific product
router.get("/:productId/details", (req, res) => {
	
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/update", auth.verify, (req, res) => {
  const data = {
    productId: req.body.productId,
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }
  productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
})


// Route for archiving a product
router.put("/:id/archive", auth.verify, (req, res) => {

	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
})
// Route for activating a product

router.put("/:id/activate", auth.verify, (req, res) => {
  productController.activateProduct(req.params)
    .then((success) => {
      if (success) {
        res.status(200).json({ message: `Product with ID ${req.params.id} has been activated.` });
      } else {
        res.status(404).json({ message: `Product with ID ${req.params.id} not found.` });

      }
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send("Internal Server Error");
    });
});





module.exports = router;
